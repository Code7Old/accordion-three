<?php

#@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

defined('_JEXEC') or die;

// loads in contenthelper if needed (on pages without an article etc)
if(!class_exists('ContentHelperRoute')) require_once (JPATH_SITE . '/components/com_content/helpers/route.php');

?>

<?php
	// get all the parameters we need, defined in the xml
	$showCat = $params->get('showCat');
	$headingHeight = $params->get('headingHeight');
    $headingSize = $params->get('headingSize');
    $catSize = $params->get('catSize');
    $database = JFactory::getDbo();
    $close = $params->get('close_buttons');
    // and start an iterator.
    $i = 1;
?>

<?php if($headingHeight != "" || $headingSize != "" || $catSize != "" ): ?>
<?php // if any of these items have a value defined then we need to output some CSS ?>
	<style type="text/css">
		<?php if($headingHeight != "" ): ?>
			.fancypantsaccordionholder > ul > li { height: <?php echo $headingHeight; ?>px; list-style: none; }
		<?php endif; ?>
		<?php if($headingHeight != "" && $headingSize == "" ): ?>
			.fancypantsaccordionholder > ul > li .headerlink { line-height: <?php echo $headingHeight; ?>px; }
		<?php elseif ($headingHeight == "" && $headingSize != "" ): ?>
			.fancypantsaccordionholder > ul > li .headerlink { font-size: <?php echo $headingSize; ?>px; }
		<?php elseif($headingHeight != "" && $headingSize !="" ): ?>
			.fancypantsaccordionholder > ul > li .headerlink { font-size: <?php echo $headingSize; ?>px; line-height: <?php echo $headingHeight; ?>px; }
		<?php endif; ?>
		<?php if($showCat == '1' && $catSize == ""): ?>
			.fancypantsaccordionholder > ul > li .headerlink p{ display: inline; }
		<?php elseif($showCat == '1' && $catSize != ""): ?>
			.fancypantsaccordionholder > ul > li .headerlink p{ display: inline; font-size: <?php echo $catSize; ?>px; }
		<?php endif; ?>
	</style>
<?php endif; ?>

<div class="fancypantsaccordionholder <?php echo $moduleclass_sfx;?>">

	<ul class="accordion">

		<?php foreach ($list as $item): ?>

		<?php
			// here we get the category id and id of the article
			$categoryID = $item->catid;
			$itemID = $item->id;

			// we get the title of the category it's in 
			$query = $database->getQuery(true);
			$query->select('title');
			$query->from('#__categories');
			$query->where('id = '.$categoryID);
			$database->setQuery($query);
			$catname = $database->loadResult();

			// then we generate a nice URL using the ID and Category ID
			$url = JRoute::_(ContentHelperRoute::getArticleRoute($item->id, $item->catid));
		?>

		<li class="accordion-item">
			<?php // we use the iterator to add an id to the href because someone wanted one ?>
			<a href="#" id="c7-accordion-heading-<?php echo $i ;?>" class="headerlink">
			<?php if ($showCat == '0'):
					// echo the title or the title and the category name if they want it
					echo htmlspecialchars($item->title);
				else:
					echo htmlspecialchars($item->title)." <p>(".$catname.")</p>" ;
				endif;
			?>
				<span class="acc-arrow">Open or Close</span>
			</a>

			<div class="acc-content">
				<?php

					// get the items intro text (before the readmore)
					$intro = $item->introtext;

					// if they want to render content plugins we run the intro variable through that
					if($params->get('render','1')){
						JPluginHelper::importPlugin('content');
						$intro = JHtml::_('content.prepare', $intro, '', 'mod_custom.content');
					}

					// then we echo it
					echo $intro;

					// if they want to show the readmore we render that out
					if($item->readmore != 0){
						echo "<a href='". $url ."'>Read More</a>";
					}

					// and the same with close buttons
                    if($close == '1'){
                        echo '<button class="close">Close this</button>';
                    }

				?>
			</div>
		</li>

		<?php $i++; ?>

		<?php endforeach; ?>
	</ul>

</div>

<script type="text/javascript">
	<?php // we initialise the accordion here. We have to do it inline so we can use their variables provided from the admin panel in the javascipt ?>
	jQuery(function() {
		var hello = jQuery('.fancypantsaccordionholder').accordion({
			oneOpenedItem:true,
			speed:300,
			scrollSpeed:300
			<?php
				if($params->get('firstopen') == 1){
					if($params->get('whichopen')){
						echo ",open:".$params->get('whichopen');
					} else {
						echo ",open:0";
					}
				}
			?>
		});

		<?php if($close == '1'){ ?>
        	jQuery('.close').on('click', function(){
            	var open_item = jQuery(this).parents('.acc-open');
				open_item.children('a').click();
        	});
		<?php } ?>
    });

</script>
