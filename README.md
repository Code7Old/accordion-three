Code7 Responsive Accordion Module is a Joomla 2.5+ accordion module that uses jQuery to display articles in a responsive and, frankly, good looking module.

Features:
* Ability to load in jQuery with noconflict so that you're lovely mootools modules/components keep on truckin'.
* Displays articles from a selected category.
* Choose to limit the amount of articles.
* Order by recent first, ascending/descending by ID or randomly.

It's a simple module and we're not hiding that, it's lovely, it's to the point and it's simple to use.

Based on the awesome work at http://tympanus.net/codrops/2011/10/12/flexible-slide-to-top-accordion/.

CHANGELOG
---------

Previous changes can be found here:

v3.0
* Added a close button option to the accordion items
* Added proper language file support (finally)
* Removed inline styles and moved into style block at start of module output
* General tidy of provided CSS and HTML as well minification of CSS and JS, reduced http requests

v2.0.4
* Fixed CSS so that list styles only apply to the accordion and not it's children. Specificity kids. It's where it's at.

v2.0.3
* Made XML file neater so options are broken down more
* Made CSS mobile first (not that anyone would notice) and removed some unnecessary CSS relating to fonts and link hovers

v2.0.2
* Added $i++ in because I forgot ಠ_ಠ

v2.0.1
* Loads in contenthelper if needed (on pages without an article etc)
* Added an ID to each link heading (requested)

v2.0
* Joomla 3 compatibility
* If you've chosen for an article to start open **and** have pop to top of open article on this will no longer jump to the first item on page load.
* Added option to choose which item in the list starts open if you want to do that.
