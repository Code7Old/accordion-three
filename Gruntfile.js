module.exports = function(grunt) {

	// because why not
	"use strict";

	// 1. All configuration goes here
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),

		concat: {
		    wScroll: {
		        src: [
		        	'assets/js/jquery.easing.1.3.js',
		            'assets/js/jquery.accordion.js'
		        ],
		        dest: 'assets/js/accordion_with_scroll.js'
		    },
		    woScroll: {
		        src: [
		        	'assets/js/jquery.easing.1.3.js',
		            'assets/js/jquery.accordion_noscroll.js'
		        ],
		        dest: 'assets/js/accordion_with_no_scroll.js'
		    },
		},

		uglify: {
			build: {
				files: {
					"assets/js/accordion_with_scroll.min.js": "assets/js/accordion_with_scroll.js",
					"assets/js/accordion_with_no_scroll.min.js": "assets/js/accordion_with_no_scroll.js"
				}
			}
		},

		sass: {
			global: {
				options: {
					style: "compressed",
					precision: 10
				},
				files: {
					"assets/css/style-unprefixed.css": "assets/scss/style.scss"
				}
			}
		},

		autoprefixer: {
			global: {
				src: "assets/css/style-unprefixed.css",
				dest: "assets/css/style.css"
			}
		},

		watch: {
			options: {
				spawn: false
			},
			js: {
				files: ["/assets/js/*.js"],
				tasks: ["contact", "uglify"]
			},
			css: {
				files: ["assets/scss/*.scss"],
				tasks: ["sass", "autoprefixer"]
			},
			compression: {
				files: ['assets/**/*', 'language/**/*', 'tmpl/**/*', 'helper.php', 'mod_fancypantsaccordion.php', 'mod_fancypantsaccordion.xml'],
				tasks: ["compress"]
			}
		},

		compress: {
			main: {
				options: {
					archive: "mod_code7ResponsiveAccordion-x-x-x.zip",
					pretty: true,
					mode: 'zip'
				},
				files: [
					{expand: true, src: ['assets/css/style.css'], dest: '/'},
					{expand: true, src: ['assets/css/index.html'], dest: '/'},
					{expand: true, src: ['assets/js/accordion_with_scroll.min.js'], dest: '/'},
					{expand: true, src: ['assets/js/accordion_with_no_scroll.min.js'], dest: '/'},
					{expand: true, src: ['assets/js/index.html'], dest: '/'},
					{expand: true, src: ['assets/img/down.png'], dest: '/'},
					{expand: true, src: ['assets/img/index.html'], dest: '/'},
					{expand: true, src: ['language/**'], dest: '/'},
					{expand: true, src: ['tmpl/**'], dest: '/'},
					{expand: true, src: ['helper.php'], dest: '/'},
					{expand: true, src: ['index.html'], dest: '/'},
					{expand: true, src: ['mod_fancypantsaccordion.php'], dest: '/'},
					{expand: true, src: ['mod_fancypantsaccordion.xml'], dest: '/'},
				]
			}
		}
    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    require("load-grunt-tasks")(grunt);

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
  	grunt.registerTask("default", ["concat", "uglify", "sass", "autoprefixer", "compress", "watch"]);

};